FLAGS += -std=c99 # Define which version of the C standard to use
FLAGS += -Wall # Enable the 'all' set of warnings
FLAGS += -Werror # Treat all warnings as error
FLAGS += -Wshadow # Warn when shadowing variables
FLAGS += -Wextra # Enable additional warnings
FLAGS += -O2 -D_FORTIFY_SOURCE=2 # Add canary code, i.e. detect buffer overflows
FLAGS += -fstack-protector-all # Add canary code to detect stack smashing
FLAGS += -D_POSIX_C_SOURCE=201112L -D_XOPEN_SOURCE # feature_test_macros for getpot and getaddrinfo

IN = in.txt
OUT = encrypted_file
DECRYPT = out.txt

all : key crypt clean

crypt: generate.o
	@gcc -c crypt.c -o crypt.o $(FLAGS)
	@gcc crypt.o generate.o -o crypt $(FLAGS)

key: generate.o
	@gcc -c key.c -o key.o $(FLAGS)
	@gcc key.o generate.o -o key $(FLAGS)

generate.o: generate.h generate.c
	@gcc -c generate.c -o generate.o


clean :
	@rm *.o

start :
	./crypt -i $(IN)

md5:
	md5sum $(IN) $(DECRYPT)

test:
	./crypt -i $(IN)
	./crypt -i $(OUT) -o $(DECRYPT)
	md5sum $(IN) $(DECRYPT)

mrproper:
	@rm crypt key encrypted_file key_*

install : all
	@echo "Copy of binaries files in /usr/local/bin"
	myinstall key
	myinstall crypt
