#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "generate.h"

void program_error(char *str);
int strcasecmp(char const *a, char const *b);
char tolower(unsigned char ch);
void questionOverwrite(int e);
int questionKey(int e);
char nextCharCode();
void analyseParametres(int argc, char const *argv[]);
void printHelp();
void date(char* time_buffer);

#define KEY_FILE 1
#define KEY_PASSWORD 2
#define KEY_AUTO 3
#define MAX_TRY  3
#define KEY_SIZE 32
