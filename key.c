#include "key.h"

int main(int argc, char const *argv[]){

    if (argc != 3 && argc != 2) {
        printf("Invalid arguments\n");
        printf("Syntax : key sizeOfKey (FileOut)\n");
        return 0;
    }

    int bts = atoi(argv[1]);
    char * buffer = (char*)malloc(bts*sizeof(char));
    generate(bts,buffer);
    char *out;
    if (argc == 3) {
        out = (char*)malloc(strlen(argv[2])*sizeof(char));
        if (out==NULL) {
            return 0;
        }
        strcpy(out,argv[2]);
    }

    int file_out;
    if (argc == 3) {
        file_out = open(out, O_WRONLY | O_CREAT , 0644);
    }
    else{
        file_out = STDOUT_FILENO;
    }
    write(file_out,buffer,(size_t)bts);
    free(buffer);
    return 0;
}
