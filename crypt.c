#include "crypt.h"

char *code;
char *add_in;
char *add_out;
char *add_key;
int decrypt;
int file_in;
int file_out;
int file_key;

int err;
size_t indice;
size_t codeLenght;

int out_arg;
int key_arg;
int in_arg;

int main(int argc, char const *argv[])
{
    analyseParametres(argc,argv);

    int cont = 1;
    while(cont){
        char *buffer = malloc(sizeof(char));
        if (buffer==NULL) {
            printf("Erreur malloc\n");
            return 0;
        }
        int length = read(file_in,buffer,(size_t)1);
        if (length>0) {
            char c_code = nextCharCode();
            char c_in = *buffer;
            char b = (c_in^c_code);
            void * ptr;
            ptr = &b;
            //write(int fildes, const void *buf, size_t nbyte);
            err = write(file_out,ptr,(size_t)1);
            if(err==-1){
                program_error("WRITE");
                remove(add_out);
                return 0;
            }
        }
        else{
            cont = 0;
        }
        free(buffer);
    }
    return 0;
}


void program_error(char *str){
    printf("Error : %s\n",str);
}

int strcasecmp(char const *a, char const *b){
    for (;; a++, b++) {
        int d = tolower(*a) - tolower(*b);
        if (d != 0 || !*a)
        return d;
    }
}

char tolower(unsigned char ch) {
    if (ch >= 'A' && ch <= 'Z')
    ch = 'a' + (ch - 'A');
    return ch;
}

void questionOverwrite(int e){
    if (e == 0) {
        exit(-1);
    }
    char str[10];
    printf("The file %s already exists. Do you want overwrite it (y/n): ",add_out);
    fgets(str,10,stdin);
    if (strcasecmp(str,"y\n") == 0 || strcasecmp(str,"yes\n") == 0 || strcasecmp(str,"oui\n") == 0 || strcasecmp(str,"o\n") == 0) {
        remove(add_out);
    }
    else if(strcasecmp(str,"n\n") == 0 || strcasecmp(str,"no\n") == 0 || strcasecmp(str,"non\n") == 0){
        exit(-1);
    }
    else{
        questionOverwrite(e-1);
    }
}

int questionKey(int e){
    if (e == 0) {
        //Automatic KEY
        code = malloc(KEY_SIZE*sizeof(char));
        generate(KEY_SIZE,code);

        char* namekey = (char*)malloc((12+1+4)*sizeof(char));
        date(namekey);
        file_key = open(namekey, O_WRONLY | O_CREAT , 0644);
        write(file_key,code,KEY_SIZE);
        printf("A key has been gereted with name : %s\n",namekey);

        return KEY_AUTO;

    }
    char str[5];
    printf("No key file.\nPress 1 and generate an automatic %d bits key (Only for encrypt)\nPress 2 and enter a password\nYour choice : ",KEY_SIZE*8);
    fgets(str,5,stdin);
    if (strcasecmp(str,"1\n") == 0) {
        questionKey(0);
        return -1;
    }
    else if(strcasecmp(str,"2\n") == 0){
        //Password
        printf("Password : ");
        code = (char*)malloc(sizeof(char)*100);
        fgets(code,100,stdin);
        return KEY_PASSWORD;
    }
    else{
        questionKey(e-1);
        return -1;
    }
}

void date(char* time_buffer){
    time_t temps;
    struct tm date;

    time(&temps);
    date=*gmtime(&temps);

    int yea = date.tm_year-100;
    int mon = date.tm_mon+1;
    int day = date.tm_mday;
    int hou = date.tm_hour+1;
    int min = date.tm_min;
    int sec = date.tm_sec;

    int i = 0;

    sprintf(time_buffer, "key_%d", yea);
    i = i+6;
    if (mon<10) {
        sprintf(time_buffer+i, "%d",0);
        i = i+1;
        sprintf(time_buffer+i, "%d",mon);
        i = i+1;
    }
    else{
        sprintf(time_buffer+i, "%d", mon);
        i = i+2;
    }
    if (day<10) {
        sprintf(time_buffer+i, "%d",0);
        i = i+1;
        sprintf(time_buffer+i, "%d",day);
        i = i+1;
    }
    else{
        sprintf(time_buffer+i, "%d", day);
        i = i+2;
    }
    if (hou<10) {
        sprintf(time_buffer+i, "%d",0);
        i = i+1;
        sprintf(time_buffer+i, "%d",hou);
        i = i+1;
    }
    else{
        sprintf(time_buffer+i, "%d", hou);
        i = i+2;
    }
    if (min<10) {
        sprintf(time_buffer+i, "%d",0);
        i = i+1;
        sprintf(time_buffer+i, "%d",min);
        i = i+1;
    }
    else{
        sprintf(time_buffer+i, "%d", min);
        i = i+2;
    }
    if (sec<10) {
        sprintf(time_buffer+i, "%d",0);
        i = i+1;
        sprintf(time_buffer+i, "%d",sec);
        i = i+1;
    }
    else{
        sprintf(time_buffer+i, "%d", mon);
    }
}

char nextCharCode(){
    if (indice==codeLenght){
        indice = 0;
    }
    return code[indice];
}

void analyseParametres(int argc, char const *argv[]){
    int i;
    int result;

    if (argc==1) {
        printHelp();
    }
    if (strcmp(argv[1],"-h")==0 || strcmp(argv[1],"--help")==0) {
        printHelp();
    }

    //IN
    for (i = 0; i < argc-1; i++) {
        if (strcmp(argv[i],"-i")==0) {
            add_in = malloc(strlen(argv[i+1]));
            strcpy(add_in,argv[i+1]);
            result = access (add_in, F_OK);
            if (result!=0) {
                printf("Input file doesn't exist\n");
                exit(-1);
            }
            in_arg = 1;
            i = argc;
        }
    }

    if (!in_arg) {
        printf("No input file\n");
        printHelp();
        exit(-1);
    }
    file_in = open(add_in,O_RDONLY);


    //KEY
    key_arg = 0;
    for (i = 0; i < argc-1; i++) {
        if (strcmp(argv[i],"-k")==0) {
            add_key = malloc(strlen(argv[i+1]));
            strcpy(add_key,argv[i+1]);
            result = access (add_key, F_OK);
            if (result!=0) {
                printf("Key file doesn't exist\n");
            }
            else{
                key_arg = KEY_FILE;
            }
            i = argc;
        }
    }
    if (key_arg == KEY_FILE) {
        struct stat *key_stat = (struct stat *) malloc(sizeof(struct stat));
        stat(add_key,key_stat);
        codeLenght = key_stat->st_size;
        file_key = open(add_key,O_RDONLY);
        code = (char*)malloc(codeLenght);
        if (code==NULL) {
            printf("Error malloc : FileKey too big\n");
            exit(-1);
        }
        read(file_key,code,codeLenght);
        free(key_stat);
    }

    if (key_arg==0) {
        key_arg = questionKey(MAX_TRY);
    }

    //OUT
    out_arg = 0;
    for (i = 0; i < argc-1; i++) {
        if (strcmp(argv[i],"-o")==0) {
            add_out = malloc(strlen(argv[i+1]));
            strcpy(add_out,argv[i+1]);
            out_arg = 1;
            i = argc;
        }
    }
    if (!out_arg) {
        add_out = (char*)malloc(strlen("encrypted_file"));
        strncpy(add_out,"encrypted_file",strlen("encrypted_file"));
    }
    result = access (add_out, F_OK);
    if (result==0) {
        questionOverwrite(MAX_TRY);
    }
    file_out = open(add_out, O_WRONLY | O_CREAT , 0644);

}

void printHelp() {
    printf("\n\tHELP :\n\n");
    printf("\tSyntax : \n");
    printf("\tcrypt -i inputfile\n");
    printf("\tOptionnal : \n");
    printf("\tcrypt -i inputfile -k keyfile -o output\n\n");
    printf("\t-h or --help for help\n\n");
    printf("\tFor decrypt document, just switch.\n\n");

    exit(-1);
}
